FROM maven:3.9.0-eclipse-temurin-19-alpine
RUN ["adduser","-D","-u","2000","-s","/bin/bash","job-user"]
RUN ["echo", "job-user:job","|","chpasswd"]
RUN ["chown", "-R","job-user:job-user","/var/lib/jenkins/workspace/"]
ENV HOME="/home/job-user"
ENV MAVEN_CONFIG="$HOME/.m2"
WORKDIR $HOME
USER 2000
CMD ["mvn","-X"]
