import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

public class NombreRomainsTest {
    @ParameterizedTest
    @ValueSource(ints = {1,2,3})
    @DisplayName("ÉTANT DONNÉ le nombre <x> compris entre 1 et 3, QUAND on le convertit en nombre romain, ALORS on obtient <x> fois I CAS 1 2 3")
    public void convertirUnEnChiffresRomainsDonneI(int x) {
        String nombreRomainAttendu = "I".repeat(x);
        ConvertisseurNombreArabe convertisseur = new ConvertisseurNombreArabe();
        String nombreRomain = convertisseur.convertir(x);
        Assertions.assertEquals(nombreRomainAttendu,nombreRomain);
    }
    @Test
    @DisplayName("ÉTANT DONNÉ le nombre 4, QUAND on le convertit en nombre romain, ALORS on obtient IV")
    public void convertirQuatreEnNombreRomainDonneIV() {
        int nombreArabe = 4;
        String nombreRomainAttendu = "IV";
        ConvertisseurNombreArabe convertisseur = new ConvertisseurNombreArabe();
        String nombreRomain = convertisseur.convertir(nombreArabe);
        Assertions.assertEquals(nombreRomainAttendu,nombreRomain);
    }
    @ParameterizedTest
    @ValueSource(ints = {5,6,7,8})
    @DisplayName("ÉTANT DONNÉ le nombre <x> >= 5 <=8, QUAND on le convertit en nombre romain, ALORS on obtient V + (<x> - 5) bâtons")
    public void convertirUnNombreXEntreCinqetHuitEnNombreRomainDonneVPlusXMoinsCinqBatons(int x) {
        String nombreRomainAttendu = "V" + "I".repeat(x - 5);
        ConvertisseurNombreArabe convertisseur = new ConvertisseurNombreArabe();
        String nombreRomain = convertisseur.convertir(x);
        Assertions.assertEquals(nombreRomainAttendu,nombreRomain);
    }
    @Test
    @DisplayName("ÉTANT DONNÉ le nombre 9, QUAND on le convertit en nombre romain, ALORS on obtient IX")
    public void convertirNeufEnNombreRomainDonneIX() {
        int nombreArabe = 9;
        String nombreRomainAttendu = "IX";
        ConvertisseurNombreArabe convertisseur = new ConvertisseurNombreArabe();
        String nombreRomain = convertisseur.convertir(nombreArabe);
        Assertions.assertEquals(nombreRomainAttendu,nombreRomain);
    }
    @Test
    @DisplayName("ÉTANT DONNÉ le nombre 10, QUAND on le convertit en nombre romain, ALORS on obtient X")
    public void convertirDixEnNombreRomainDonneX() {
        int nombreArabe = 10;
        String nombreRomainAttendu = "X";
        ConvertisseurNombreArabe convertisseur = new ConvertisseurNombreArabe();
        String nombreRomain = convertisseur.convertir(nombreArabe);
        Assertions.assertEquals(nombreRomainAttendu,nombreRomain);
    }
    @Test
    @DisplayName("ÉTANT DONNÉ le nombre 11, QUAND on le convertit en nombre romain, ALORS on obtient XI")
    public void convertirOnzeEnNombreRomainDonneXI() {
        int nombreArabe = 11;
        String nombreRomainAttendu = "XI";
        ConvertisseurNombreArabe convertisseur = new ConvertisseurNombreArabe();
        String nombreRomain = convertisseur.convertir(nombreArabe);
        Assertions.assertEquals(nombreRomainAttendu,nombreRomain);
    }
    @Test
    @DisplayName("ÉTANT DONNÉ le nombre 12, QUAND on le convertit en nombre romain, ALORS on obtient XII")
    public void convertirDouzeEnNombreRomainDonneXII() {
        int nombreArabe = 12;
        String nombreRomainAttendu = "XII";
        ConvertisseurNombreArabe convertisseur = new ConvertisseurNombreArabe();
        String nombreRomain = convertisseur.convertir(nombreArabe);
        Assertions.assertEquals(nombreRomainAttendu,nombreRomain);
    }
    @Test
    @DisplayName("ÉTANT DONNÉ le nombre 13, QUAND on le convertit en nombre romain, ALORS on obtient XIII")
    public void convertirTreizeEnNombreRomainDonneXIII() {
        int nombreArabe = 13;
        String nombreRomainAttendu = "XIII";
        ConvertisseurNombreArabe convertisseur = new ConvertisseurNombreArabe();
        String nombreRomain = convertisseur.convertir(nombreArabe);
        Assertions.assertEquals(nombreRomainAttendu,nombreRomain);
    }
}