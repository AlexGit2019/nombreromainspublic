public class ConvertisseurNombreArabe {

    public String convertir(int nombreArabe) {
        return nombreArabe == 1 ? "I" : nombreArabe == 2 ? "II" :
                nombreArabe == 3 ? "III" : nombreArabe == 4 ? "IV" :
                        nombreArabe == 5 ? "V" : nombreArabe == 6 ? "VI" : nombreArabe == 7 ? "VII" : nombreArabe == 8 ? "VIII" :
                                nombreArabe == 9 ? "IX" : nombreArabe == 10 ? "X" : nombreArabe == 11 ? "XI" :
                                        nombreArabe == 12 ? "XII" : nombreArabe == 13 ? "XIII" : "";
    }
}
